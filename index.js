console.log("Hello Caps")



//Customer
class Customer {
    constructor(email){
        this.email = email
        this.cart = new Cart
        this.orders = []
    }

    //methods
    checkOut(products, totalAmount){
        if(this.cart.length !== 0){
            this.orders.push({
                products: products,
                totalAmount: this.cart.computeTotal().totalAmount
                })
        }
        return this
    }
}

//sample data


// Cart
class Cart {
    constructor(){
        this.contents = []
        this.totalAmount = 0
    }

    //methods
    addToCart(product, quantity){
        this.contents.push({
            product: product,
            quantity: quantity
            })
        return this
    }
    showCartContents(){
        console.log(this.contents)
        return this
    }


    updateProductQuantity(name, quantity){
        this.contents.find(prod =>
            prod.product.name === name).quantity = quantity
                return this
    }



    clearCartContents(){
        this.contents = [];
        this.quantity = 0;

        return this
    }
  
    computeTotal(){
		this.totalAmount = 0

		if(this.contents.length > 0){
			this.contents.forEach(item => {
				this.totalAmount = this.totalAmount + (item.product.price * item.quantity)
			})
		}

		return this
	}

}

//Product

class Product {
    constructor(name, price){
        this.name = name
        this.price = price
        this.isActive = true
    }

    //methods
    archive(){
       this.isActive = false 
    }
    updatePrice(price){
        this.price = price
        return this
    }
}









// const john = new Customer("john@mail.com")
// const prodA = new Product("soap", 9.99)
// const prodB = new Product("shampoo", 19.99)
// prodA.updatePrice(12.99)
// prodA.archive()
// john.cart.addToCart(prodA, 3)
// john.cart.addToCart(prodB, 10)
// john.cart.showCartContents()
// //john.cart.updateProductQuantity("soap", 5)
// //john.cart.clearCartContents()
// john.cart.computeTotal()
// john.checkOut()